# Real Life Reinforcement Learning with Servos

A [Reinforcement Learning Book](https://rl-book.com/?utm_source=git&utm_medium=repository&utm_campaign=real-life-reacher) project by [Winder Research](https://winderresearch.com/?utm_source=git&utm_medium=repository&utm_campaign=real-life-reacher).

| `Real life servos` | `Easy to understand policies` |
|---|---|
|![](images/moves.gif)|![](images/policy_random_goal.png)|

## Overview

A very simple example of using RL in real life. The goal was to mimic one of the popular OpenAI Gym simulations and the simplest I could come up with was using servo motors to perform a reacher like task. You can read more about it in my [book on reinforcement learning](https://rl-book.com/?utm_source=git&utm_medium=repository&utm_campaign=real-life-reacher).

I have created simulators, but for maximum entertainment you will need:

- 1x LX-16A servo motors
- 1x BusLinker V2.4 serial servo driver board
- An extra power supply to power the motors
- A USB cable

Plug them all in and run the `ppo_policy.py` file.

## Reproduction of Results

- Simple random simulation: `python ppo_policy.py trained_policies/ppo_RealLifeReacher-v0_sim.zip --total_timesteps=1025 --plot_policy --monitoring_dir=./monitoring --seed=42`
- Simple random real servos: `python ppo_policy.py trained_policies/ppo_RealLifeReacher-v0_real.zip --total_timesteps=1025 --plot_policy --monitoring_dir=./monitoring --seed=42 --for_real`

## Credits

Icons made by <a href="https://www.flaticon.com/authors/payungkead" title="Payungkead">Payungkead</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>