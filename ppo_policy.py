import argparse
import os
from pathlib import Path
import sys
from typing import Optional

import gym
from loguru import logger
from lx16a.protocol import Protocol
import serial
from stable_baselines import PPO2
from stable_baselines.common.evaluation import evaluate_policy
from stable_baselines.common.schedules import LinearSchedule
import tensorflow as tf
import yaml

import gym_real_life_reacher
from gym_real_life_reacher.envs.controller import SimulationController, ServoController
from gym_real_life_reacher.wrappers import MaxTimeStepsSafetyValve
from gym_real_life_reacher.monitoring import PlotPolicyCallback
from gym_real_life_reacher.utility import seed

# Remove tensorflow warnings
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

# Remove servo debug logs
logger.remove()
logger.add(sys.stdout, level=os.getenv("LOG_LEVEL", "INFO"))


def main(args: dict):
    # Create environment
    environment_id = "RealLifeReacher-v0"
    c = SimulationController()
    if args["for_real"]:
        ser = serial.Serial("/dev/cu.usbserial-1420", 115200, timeout=1)
        p = Protocol(serial=ser)
        c = ServoController(protocol=p)
    env = gym.make(environment_id, controller=c, goal_position=args["goal"])
    env = MaxTimeStepsSafetyValve(
        env, max_timesteps=100
    )  # Just in case, limit the num steps per episode
    seed(args["seed"], env)

    # Create agent
    with open("hyperparams/ppo.yaml", "r") as f:
        hyperparams = yaml.safe_load(f)[environment_id]
        logger.info(f"Policy hyperparameters: {hyperparams}")

    model = PPO2(
        "MlpPolicy",
        env,
        verbose=1,
        tensorboard_log="./log/",
        seed=args["seed"],
        **hyperparams,
    )
    callback = (
        PlotPolicyCallback(
            plot_freq=25, monitoring_dir=args["monitoring_dir"], goal=args["goal"]
        )
        if args["plot_policy"]
        else None
    )

    # Train agent
    if args["model_path"] and not args["model_path"].exists():
        logger.info("Training policy")
        try:
            model.learn(total_timesteps=args["total_timesteps"], callback=callback)
        except KeyboardInterrupt:
            pass
        finally:
            logger.info(f'Saving trained model to {args["model_path"]}, wait!')
            model.save(str(args["model_path"]))

    # Evaluate trained agent
    logger.info(f'Testing policy from {args["model_path"]}')
    model.load_parameters(str(args["model_path"]))
    seed(args["seed"], env)
    mean_reward, std_reward = evaluate_policy(
        model, env, n_eval_episodes=25, deterministic=True
    )
    logger.info(f"Trained policy reward = {mean_reward:.2f} +/- {std_reward}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Train a real life reacher!")
    parser.add_argument(
        "--plot_policy",
        action="store_true",
        help="If enabled, plot the current policy.",
    )
    parser.add_argument(
        "--goal", type=float, help="Specify a fixed goal. This is easier to learn.",
    )
    parser.add_argument(
        "--total_timesteps",
        type=int,
        help="Total number of timesteps to train the model. Goal version needs about 20.",
        default=200,
    )
    parser.add_argument(
        "model_path",
        type=Path,
        help="Path to policy. If it exists, load and evaluate. If not, train and save. (path to zip file)",
    )
    parser.add_argument(
        "--monitoring_dir", type=Path, help="Directory to save monitoring files. (dir)",
    )
    parser.add_argument(
        "--seed", type=int, default=42, help="Random seed.",
    )
    parser.add_argument(
        "--for_real",
        action="store_true",
        help="Use a real life stepper motor! Otherwise simulate.",
    )
    main(vars(parser.parse_args()))
