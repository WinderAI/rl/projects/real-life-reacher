import os
import sys
from typing import Optional

import gym
from loguru import logger
import optuna
from stable_baselines import PPO2
from stable_baselines.common.evaluation import evaluate_policy
import tensorflow as tf
import yaml

import gym_real_life_reacher
from gym_real_life_reacher.envs.controller import SimulationController
from gym_real_life_reacher.utility import seed
from gym_real_life_reacher.wrappers import MaxTimeStepsSafetyValve

# Remove tensorflow warnings
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

# Remove servo debug logs
logger.remove()
logger.add(sys.stdout, level=os.getenv("LOG_LEVEL", "WARNING"))

seed_value = 42


def make_env(environment_id: str, goal: Optional[float] = None) -> gym.Env:
    """Creates the real life reacher environment using my servo library. See the readme for details.

    Args:
        environment_id (str): Environment ID
        goal (Optional[float]): A fixed target position. If None then it uses a random target every episode.
    """
    c = SimulationController()
    env = gym.make(environment_id, controller=c, goal_position=goal)
    env = MaxTimeStepsSafetyValve(
        env, max_timesteps=100
    )  # Just in case, limit the num steps per episode
    return env


def ppo_hyperparams(trial):
    learning_rate = trial.suggest_categorical("learning_rate", [0.001, 0.005, 0.01])
    nminibatches = trial.suggest_categorical("nminibatches", [2, 4, 8])
    n_steps = trial.suggest_categorical("n_steps", [32, 64, 128])
    architectures = {}
    for outer in [4, 8, 16]:
        for inner in [4, 8, 16]:
            architectures[f"{outer}_{inner}"] = [
                dict(pi=[outer, inner], vf=[outer, inner])
            ]
    net_arch = trial.suggest_categorical("net_arch", architectures.keys())
    net_arch = architectures[net_arch]
    return {
        "learning_rate": learning_rate,
        "nminibatches": nminibatches,
        "n_steps": n_steps,
        "policy_kwargs": dict(net_arch=net_arch),
    }


def objective(trial):
    environment_id = "RealLifeReacher-v0"
    env = make_env(environment_id)

    with open("hyperparams/ppo.yaml", "r") as f:
        hyperparams = yaml.safe_load(f)[environment_id]
        logger.info(f"Policy hyperparameters: {hyperparams}")
    hyperparams.update(ppo_hyperparams(trial))
    model = PPO2(
        "MlpPolicy",
        env,
        verbose=0,
        tensorboard_log="./log/",
        seed=seed_value,
        **hyperparams,
    )
    seed(seed_value, env)
    model.learn(total_timesteps=2000)
    seed(seed_value, env)
    mean_reward, std_reward = evaluate_policy(
        model, env, n_eval_episodes=25, deterministic=True
    )

    return -mean_reward


study = optuna.create_study(
    study_name="study", storage="sqlite:///example.db", load_if_exists=True,
)

try:
    study.optimize(objective, n_trials=250)
except KeyboardInterrupt:
    pass

print("Number of finished trials: ", len(study.trials))

print("Best trial:")
trial = study.best_trial

print("Value: ", trial.value)

print("Params: ")
for key, value in trial.params.items():
    print("    {}: {}".format(key, value))
