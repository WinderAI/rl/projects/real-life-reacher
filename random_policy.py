import gym
import gym_real_life_reacher
from gym_real_life_reacher.envs.controller import Controller
import serial
from lx16a.protocol import Protocol

ser = serial.Serial("/dev/cu.usbserial-1420", 115200, timeout=1)
p = Protocol(serial=ser)
c = Controller(protocol=p)
env = gym.make("RealLifeReacher-v0", controller=c)

done = False
while not done:
    obs, reward, done, _ = env.step(env.action_space.sample())
    print(obs, reward, done)
