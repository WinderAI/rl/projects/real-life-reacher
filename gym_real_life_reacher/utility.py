from loguru import logger
import numpy as np
import random
import tensorflow as tf


def seed(value, env):
    logger.debug(f"Setting random seeds with value {value}.")
    np.random.seed(value)
    env.seed(value)
    env.action_space.seed(value)
    random.seed(value)
    tf.random.set_random_seed(value)
