from gym.envs.registration import register

register(
    id="RealLifeReacher-v0",
    entry_point="gym_real_life_reacher.envs:SimpleRealLifeReacher",
)
