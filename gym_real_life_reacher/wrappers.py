import gym


class MaxTimeStepsSafetyValve(gym.Wrapper):
    """Sometimes episodes go on for too long and get stuck. This safety valve prevents that from happening."""

    def __init__(self, env: gym.Env, max_timesteps=2000):
        """Instantiate class.

        Parameters
        ----------
        env : gym.Env
            The environment
        max_timesteps : int, optional
            The maximum number of timesteps the environment is allowed to run for, by default 2000
        """
        super(MaxTimeStepsSafetyValve, self).__init__(env)
        self.max_timesteps = max_timesteps
        self.counter = 0

    def reset(self, **kwargs):
        self.counter = 0
        return self.env.reset(**kwargs)

    def step(self, action):
        observation, reward, done, info = self.env.step(action)
        self.counter += 1
        if self.counter > self.max_timesteps:
            done = True
        return observation, reward, done, info
