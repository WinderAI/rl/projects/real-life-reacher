from pathlib import Path
from typing import Optional

import matplotlib.pyplot as plt
import numpy as np
from stable_baselines.common.callbacks import BaseCallback

plt.rcParams["figure.figsize"] = (6, 4)
plt.style.use(str(Path(__file__).parent / "PaperDoubleFig.mplstyle"))
forground_colours = [
    "#111111",
    "#666666",
    "#CCCCCC",
]


class PlotPolicyCallback(BaseCallback):
    def __init__(
        self,
        plot_freq: int = 100,
        monitoring_dir: Optional[Path] = None,
        goal: Optional[float] = None,
        verbose=1,
    ):
        super(PlotPolicyCallback, self).__init__(verbose)
        self.plot_freq = plot_freq
        self.monitoring_dir = monitoring_dir
        self.monitoring_dir.mkdir(parents=True, exist_ok=True)
        self.goal = goal
        self._n_pixels = 50
        self.im = plt.imshow(
            np.random.randn(self._n_pixels, self._n_pixels),
            origin="lower",
            interpolation="none",
            extent=[-1, 1, -1, 1],
            cmap="Greys",
            vmin=-1,
            vmax=1,
        )
        plt.ylabel("Observation")
        plt.xlabel("Goal")
        cbar = plt.colorbar()
        cbar.set_label("Action", rotation=270)

    def _on_step(self) -> bool:
        if self.n_calls % self.plot_freq == 0:
            if self.goal:
                plt.title(f"For goal {self.goal}, # timesteps = {self.num_timesteps}")
            else:
                plt.title(f"# timesteps = {self.num_timesteps}")
            if self.goal:
                plt.cla()
                x_arr = np.linspace(-1, 1, 100)
                result = np.array(
                    [self.model.predict([x, -1], deterministic=True)[0] for x in x_arr]
                )
                plt.plot(x_arr, result, color=forground_colours[0], linestyle="solid")
                plt.xlim([-1, 1])
                plt.ylim([-1, 1])
                plt.xlabel("Observation")
                plt.ylabel("Policy")
            else:
                xx, yy = (
                    np.linspace(-1, 1, self._n_pixels),
                    np.linspace(-1, 1, self._n_pixels),
                )
                result = np.array(
                    [
                        self.model.predict([x, y], deterministic=True)[0]
                        for x in xx
                        for y in yy
                    ]
                )
                z = result.reshape(xx.shape[0], yy.shape[0])
                self.im.set_array(z)

            if self.monitoring_dir:
                plt.savefig(
                    self.monitoring_dir / f"policy_{self.num_timesteps}.png", dpi=300
                )
            plt.draw()
            plt.pause(0.001)

        return True
