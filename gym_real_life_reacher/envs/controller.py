"""Interface to Servo controller."""

from abc import ABC
import numpy as np
from lx16a.protocol import Protocol
from lx16a.servo import Servo
from loguru import logger


class Controller(ABC):
    def move(self, position: float) -> None:
        raise NotImplementedError

    def observe(self) -> float:
        raise NotImplementedError


class SimulationController(Controller):
    def __init__(self) -> None:
        self._min = -1.0
        self._max = 1.0
        self.position = np.random.uniform(self._min, self._max)

    def move(self, position: float) -> None:
        position = np.clip(position, self._min, self._max)
        self.position = position

    def observe(self) -> float:
        return self.position


class ServoController(Controller):
    def __init__(self, protocol: Protocol) -> None:
        self.servo = Servo(protocol=protocol)
        self._min_raw_value = 150
        self._max_raw_value = 850
        self._servo_midpoint = 500
        self._scaling_factor = (self._max_raw_value - self._min_raw_value) / 2

    def move(self, position: float) -> None:
        position = np.clip(position, -1, 1)
        servo_position = position * self._scaling_factor + self._servo_midpoint
        servo_clipped = np.clip(
            servo_position, self._min_raw_value, self._max_raw_value
        )
        logger.debug(
            f"Desired: scaled {position}, raw {servo_position}, clipped {servo_clipped}"
        )
        try:
            self.servo.set_position_blocking(int(servo_position))
        except ConnectionError as e:
            logger.warning(e)

    def observe(self) -> float:
        servo_position = 0
        try:
            servo_position = self.servo.get_position()
        except ConnectionError as e:
            logger.warning(e)
        position = (servo_position - self._servo_midpoint) / self._scaling_factor
        logger.debug(f"Position: raw {servo_position}, scaled {position}")
        return position
