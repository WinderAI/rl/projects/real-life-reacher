from typing import Optional
import gym
import numpy as np
from gym.utils import seeding
from .controller import Controller
from gym import error, spaces
from loguru import logger


def goal_distance(goal_a, goal_b):
    return np.linalg.norm(goal_a - goal_b, axis=-1)


class SimpleRealLifeReacher(gym.Env):
    def __init__(
        self, controller: Controller, goal_position: Optional[float] = None,
    ):
        self.controller = controller
        self.goal_position = goal_position
        self.n_actions = 1
        self.reward_type = "dense"
        self.success_distance = 0.05
        self.metadata = {
            "render.modes": ["human", "rgb_array"],
        }
        self.seed()
        self.goal = self._sample_goal()
        obs = self._get_obs()
        self.action_space = spaces.Box(
            -1.0, 1.0, shape=(self.n_actions,), dtype="float32"
        )
        self.observation_space = spaces.Box(
            -np.inf, np.inf, shape=obs.shape, dtype="float32"
        )
        self.viewer = None

    # GoalEnv methods
    # ----------------------------

    def compute_reward(self, achieved_goal, goal, info):
        # Compute distance between goal and the achieved goal.
        d = goal_distance(achieved_goal, goal)
        if self.reward_type == "sparse":
            return -(d > self.distance_threshold).astype(np.float32)
        else:
            return -d

    # Env methods
    # ----------------------------

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

    def step(self, action):
        action = np.clip(action, self.action_space.low, self.action_space.high)
        self.controller.move(action)
        obs = self._get_obs()
        info = {}
        reward = self.compute_reward(obs[0:1], np.array([self.goal]), info)
        done = np.abs(reward) < self.success_distance
        if done:
            logger.info("SUCCESS!!")
        return obs, reward, done, info

    def reset(self):
        self.goal = self._sample_goal()
        self.controller.move(np.random.uniform(-1.0, 1.0))
        obs = self._get_obs()
        logger.info(f"RESET. Goal = {self.goal}, start = {obs}")
        return obs

    def close(self):
        if self.viewer is not None:
            # self.viewer.finish()
            self.viewer = None
            self._viewers = {}

    def render(self, mode="human"):
        pass

    def _sample_goal(self) -> float:
        return self.goal_position or np.random.uniform(-1.0, 1.0)

    def _get_obs(self) -> np.ndarray:
        position = self.controller.observe()
        return np.array([position, self.goal])
